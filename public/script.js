document.getElementById('start').addEventListener('click', startGame);

let board, revealed, scores, currentPlayer, firstCard, secondCard;

function startGame() {
    const rows = parseInt(document.getElementById('rows').value);
    const cols = parseInt(document.getElementById('cols').value);

    if (rows * cols % 2 !== 0) {
        alert('Liczba kart musi być parzysta!');
        return;
    }

    board = generateBoard(rows, cols);
    revealed = Array.from({ length: rows }, () => Array(cols).fill(false));
    scores = [0, 0];
    currentPlayer = 0;
    firstCard = null;
    secondCard = null;

    updateScore();
    renderBoard(rows, cols);
}

function generateBoard(rows, cols) {
    const totalCards = rows * cols;
    const pairs = totalCards / 2;
    let cards = [];
    for (let i = 1; i <= pairs; i++) {
        cards.push(i);
        cards.push(i);
    }

    shuffle(cards);

    const board = [];
    let index = 0;
    for (let i = 0; i < rows; i++) {
        board[i] = [];
        for (let j = 0; j < cols; j++) {
            board[i][j] = cards[index++];
        }
    }

    return board;
}

function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

function renderBoard(rows, cols) {
    const gameDiv = document.getElementById('game');
    gameDiv.innerHTML = '';
    gameDiv.style.gridTemplateRows = `repeat(${rows}, 1fr)`;
    gameDiv.style.gridTemplateColumns = `repeat(${cols}, 1fr)`;

    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < cols; j++) {
            const card = document.createElement('div');
            card.classList.add('card');
            card.dataset.row = i;
            card.dataset.col = j;
            card.addEventListener('click', onCardClick);
            gameDiv.appendChild(card);
        }
    }
}

function onCardClick(event) {
    const card = event.target;
    const row = card.dataset.row;
    const col = card.dataset.col;

    if (revealed[row][col] || firstCard && secondCard) return;

    revealed[row][col] = true;
    card.classList.add('revealed');
    card.textContent = board[row][col];

    if (!firstCard) {
        firstCard = { row, col, element: card };
    } else {
        secondCard = { row, col, element: card };
        checkMatch();
    }
}

function checkMatch() {
    if (board[firstCard.row][firstCard.col] === board[secondCard.row][secondCard.col]) {
        scores[currentPlayer]++;
        updateScore();
        resetCards();
    } else {
        setTimeout(() => {
            firstCard.element.classList.remove('revealed');
            secondCard.element.classList.remove('revealed');
            firstCard.element.textContent = '';
            secondCard.element.textContent = '';
            revealed[firstCard.row][firstCard.col] = false;
            revealed[secondCard.row][secondCard.col] = false;
            currentPlayer = 1 - currentPlayer;
            updateScore();
            resetCards();
        }, 1000);
    }
}

function resetCards() {
    firstCard = null;
    secondCard = null;
}

function updateScore() {
    document.getElementById('score1').textContent = scores[0];
    document.getElementById('score2').textContent = scores[1];
    document.getElementById('currentPlayer').textContent = currentPlayer + 1;
}
